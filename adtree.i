%module adtree

%{
#define SWIG_FILE_WITH_INIT
#include "adtree.h"
%}

%include "numpy.i"

%init %{
import_array();
%}


%typemap(doctype) int* "numpy.int32";
%typemap(doctype) void* "adtree";

%apply (int* IN_ARRAY1, int DIM1) {(int* data, int datalength)}
%apply (int* IN_ARRAY1, int DIM1) {(int* arities, int nvars)}
%apply (int* IN_ARRAY1, int DIM1) {(int* variables, int nvariables)}
%apply (int* INPLACE_ARRAY1, int DIM1) {(int* flatcontab, int flatcontabsize)}
%include "adtree.h"
