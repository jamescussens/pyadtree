/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *   PYADTREE Copyright (C) 2020 James Cussens                           *
 *                                                                       *
 *   This program is free software; you can redistribute it and/or       *
 *   modify it under the terms of the GNU General Public License as      *
 *   published by the Free Software Foundation; either version 3 of the  *
 *   License, or (at your option) any later version.                     *
 *                                                                       *
 *   This program is distributed in the hope that it will be useful,     *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of      *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    *
 *   General Public License for more details.                            *
 *                                                                       *
 *   You should have received a copy of the GNU General Public License   *
 *   along with this program; if not, see                                *
 *   <http://www.gnu.org/licenses>.                                      *
 *                                                                       *
 *                                                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
   Create an ADTREE for a discrete dataset

   @param rmin If count below this then create a leaflist.
   @param adtreedepthlim Limit on the depth of the ADTREE.
   @param adtreenodeslim Limit on the number of nodes in the ADTREE.
   @param data Column-major flattened version of the data: data[i*ndatapoints+j] is the value of variable i in datapoint j
   @param arities arities[i] is the arity of variable i
   @param ndatapoints Number of datapoints in data
   @return The ADTREE
 */
void* adtree(
   int rmin,             
   int adtreedepthlim,   
   int adtreenodeslim,
   int* data,            
   int datalength,           
   int* arities,     
   int nvars,
   int ndatapoints
  );

/**
   Create a (flattened) contingency table from an ADTREE

   Suppose variables 2,4,5 have possible values (0,1),(0,1,2),(0,1) (resp.) then the flattened contingency table has 2*3*2=12 integer
   values. The index for the count for joint instantiation (2=i,4=j,5=k) is 6*i+2*j+k. 

   This function should be called with an arbitrary (e.g. uninitialised) 1d numpy.int32 array which is big enough to
   contain the desired contingency table (it's OK if it's overlength). If the supplied numpy array is too short then
   this function returns -1, otherwise the size of the computed contingency table (call this "SIZE") is returned (which
   may be less than the length of the supplied numpy array) and the first SIZE elements of the array will contain the required
   contingency table.

   @param adtree The ADTREE
   @param variables The variables for the contingency table (sorted)
   @param flatcontab On entry an uninitialised array. If there is enough room in this array then on exit it will contain the desired flat contingency table
   @return -1 if supplied array too short, otherwise the length of the computed flat contingency table
 */

int contab(
   void* adtree,
   int* variables,
   int nvariables,
   int* flatcontab, 
   int flatcontabsize
  );

/**

   Free (the memory for) an ADREE

   @param adtree The ADTREE
 */
void freeadtree(
   void *adtree 
   );

