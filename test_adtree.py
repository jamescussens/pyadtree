import adtree
import numpy as np
import random
import math
import itertools
import timeit
import pandas as pd


# fix random seed for replicability
random.seed(10)

# create data with 5 independent variables with varying arities
# data is pXn matrix
p = 5
plist = list(range(p))
arities = np.array([2,3,4,2,5],dtype=np.int32)
n = 1000
data = []
for v in arities:
    weights = [random.random() for _ in range(v)]
    data.append(random.choices(range(v),weights=weights,k=n))
data = np.array(data,dtype=np.int32)
df = pd.DataFrame(np.transpose(data))

#ad tree parameters
rmin = 10
adtreedepthlim = 1000
adtreenodeslim = 1000
adt = adtree.adtree(rmin,adtreedepthlim,adtreenodeslim,
                    data.flatten(),arities,n)


def simple(vs,contab):
    contabsize = 1
    strides = []
    for v in reversed(vs):
        strides.append(contabsize)
        contabsize *= arities[v]
    strides.reverse()
    contab.fill(0)
    for i in range(n):
        idx = 0
        for j, v in enumerate(vs):
            idx += strides[j]*data[v,i]
        contab[idx] += 1
    return contab

def with_simple():
    contabsize = math.prod([arities[i] for i in plist])
    contab = np.empty(contabsize,dtype=np.int32)
    for size in range(1,6):
        for vs in itertools.combinations(plist,size):
            simple(vs,contab)

def with_pandas():
    for size in range(1,6):
        for vs in itertools.combinations(plist,size):
            df.groupby(list(vs)).count()


def with_adtree():
    contabsize = math.prod([arities[i] for i in plist])
    contab = np.empty(contabsize,dtype=np.int32)
    for size in range(1,6):
        for vs in itertools.combinations(plist,size):
            adtree.contab(adt,np.array(vs,np.int32),contab)

trials = 100
for method in 'adtree', 'pandas', 'simple':
    method_time = timeit.timeit("with_{0}()".format(method),number=trials,globals=globals())
    print('Time taken with method {0} is {1}'.format(method,method_time))

contabsize = math.prod([arities[i] for i in plist])
contab = np.empty(contabsize,dtype=np.int32)
contab2 = np.empty(contabsize,dtype=np.int32)
for size in range(1,6):
    for vs in itertools.combinations(plist,size):
        contabsize = math.prod([arities[i] for i in vs])
        csize = adtree.contab(adt,np.array(vs,np.int32),contab)
        assert contabsize == csize
        simple(vs,contab2)
        assert list(contab)[:contabsize] == list(contab2)[:contabsize]
        

adtree.freeadtree(adt)
