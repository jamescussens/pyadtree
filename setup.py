#! /usr/bin/env python

import setuptools
import numpy

try:
    numpy_include = numpy.get_include()
except AttributeError:
    numpy_include = numpy.get_numpy_include()

# adtree extension module
_adtree = setuptools.Extension(
    "_adtree",
    ["adtree.i","adtree.c"],
    include_dirs = [numpy_include],
    #undef_macros=['NDEBUG'],
    swig_opts=["-doxygen"]
)

setuptools.setup(py_modules=['adtree'],ext_modules = [_adtree] )
