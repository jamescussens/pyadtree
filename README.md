# Fast construction of contingency tables using AD-trees

AD-trees were introduced by Moore and Lee in [Cached Sufficient
Statistics for Efficient Machine Learning with Large
Datasets](https://arxiv.org/pdf/cs/9803102), Journal of Artificial
Intelligence Research 8 (1998) 67-91. An AD-tree is constructed from a
discrete dataset and allows the fast computation of arbitrary marginal
contingency tables of that dataset.

This Python package provides a single module adtree which provides 3 functions:

- adtree: Constructs an AD-tree from data
- contab: Constructs a contingency table from the AD-tree
- freeadtree: Deletes an AD-tree

You need to have numpy installed and, if you want to run the supplied
script `test_adtree.py`, also pandas.

The Python module is a wrapper round C code which does all the
necessary computations. Installation of the adtree module is currently
only available by compiling from the source, and it is necessary to
have [SWIG](http://swig.org/) installed (as well as a C compiler of
course). Installation has only been tested for Linux and the following
assumes you are using Linux. To install just do:

	 clone https://jamescussens@bitbucket.org/jamescussens/pyadtree.git
	 cd pyadtree
	 python setup.py install

SWIG uses the file numpy.i to generate a Python wrapper for the C
code, but the version of numpy.i supplied is not guaranteed to be the
latest version. If needed, you can get the latest version from:
https://github.com/numpy/numpy/blob/main/tools/swig/numpy.i

You can see how to use the module by inspecting the supplied script
`test_adtree.py`. Here is a more detailed explanation.

To construct an AD-tree (once you have imported the adtree module) you
need an assignment like this:

`adt = adtree.adtree(rmin,adtreedepthlim,adtreenodeslim,data,arities,n)`

where:

- rmin, adtreedepthlim and adtreenodeslim are positive integers which
control the size of the AD-tree. For an explanation of rmin, see
Section 5 of the Moore and Lee paper. The other two values are limits for
the depth and number of nodes in the tree, respectively. There are
currently no default values so you have to supply some
values. rmin=50, adtreedepthlim=100, adtreenodeslim=1000 are
reasonable values, but it is worth experimenting (particulaly with
rmin) to find values which give good performance.

- data is the (discrete) data as a 1d numpy array of
  dtype=np.int32. If you have p variables and n datapoints, the first
  n values should be those variable 0, the next n values for variable
  1 and so on until the last n values are for variable p-1. **If a
  variable has q possible values (ie has arity q) then the values in
  the data for this variable can only be 0,1,...,q-1.** If you have
  used different labels for the values of your discrete variables you
  will have to relabel with 0,1,...,q-1 before you can use the adtree
  module.

- arities is a 1d numpy array of dtype=np.int32 giving the arities of
  the variables

- n is the number of datapoints in the data.

Once constructed you compute contingency tables using the AD-tree like this:

`adtree.contab(adt,variables,contab)`

where

- adt is a previously constructed AD-tree

- variables is a a 1d numpy array of dtype=np.int32 listing the
  variables for the contingency table **in ascending order**. If you
  have p variables then the names of your variables have to be 0,1,
  .., p-1.

- contab is a a 1d numpy array of dtype=np.int32 which on return will
  contain the contingency table for the variables. It does not matter
  what values are contained in contab initially. contab should be long
  enough to contain the desired contingency table, but it is fine for
  it to be overlength. If it is too short then this function returns
  -1 to indicate the problem. If you want to create many contingency
  tables it can make sense to create a single numpy array which is big
  enough to contain the biggest contingency table required and then
  re-use it. On return, (unless the return value is -1) contab will
  contain the values for the contingency table in lexicographical
  order. For example, suppose we wanted a contingency table for
  variables 2,3 and 5 and these variables have arity 2,3 and 2
  respectively, then the following is a possible contingency table:

|2|3|5|Count|
|-|-|-|-----|
|0|0|0|3|
|0|0|1|4|
|0|1|0|5|
|0|1|1|0|
|0|2|0|1|
|0|2|1|2|
|1|0|0|0|
|1|0|1|0|
|1|1|0|4|
|1|1|1|6|
|1|2|0|7|
|1|2|1|3|


Assuming we have input a sufficiently long array, on return the first
12 components of contab will be
np.array([3,4,5,0,1,2,0,0,4,6,7,3]),dtype=np.int32). On a successful
execution like this the return value is the length of the contingency
table (so in this case 12).


You can delete the adtree by calling `adtree.freeadtree(adt)` where
adt is the AD-tree.

## Error checking

Apart from using a return value of -1 to indicate that a too-short
array has been sent to adtree.contab, there is currently no error
checking. So, for example, calling

`adtree.contab(adt,np.array([0,1,2,3,4,5],dtype=np.int32),contab)`

where `adt` was constructed from data with only 3 (not 5) variables,
will cause a Segmentation fault.

## Performance

Running `python test_adtree.py` will give you some idea of
performance. In this script a random data set with 5 variables and
1000 datapoints is created and all possible contingency tables are
computed using 3 different methods: AD-trees, panda's groupby.count()
approach and a simple method which inspects every datapoint. In
addition, there is a check that the AD-tree method and the simple
method compute the same contingency table in every case (they
do!). And also that adtree.contab returns the correct value (length of
computed contingency table). Here are the benchmarking results:

      Time taken with method adtree is 0.014970347998314537
      Time taken with method pandas is 9.077566915999341
      Time taken with method simple is 38.25590079200265

So in this example at least, using the `adtree` module is about 600
times faster than using pandas and over 2500 times faster than the
simple method. 

## TODO

- Add error checking
- Automatically choose good value or rmin for a particular dataset.